#!/usr/bin/python3

import sys, os, os.path, socket, time
from PyQt6 import QtGui, QtCore, QtWidgets, QtWebEngineWidgets
from PyQt6.QtCore import *
from PyQt6.QtWidgets import QApplication, QSystemTrayIcon, QApplication, QMenu, QMessageBox, QTabWidget
from PyQt6.QtGui import QAction
from PyQt6.QtWebEngineWidgets import *

from Components.SettingsComponent import SettingsComponent
from Components.ChatViewComponent import ChatViewComponent

"""
Class AngelCommClient
unofficial (more a personal :) ) whatsapp client for linux
it just takes web whatsapp into chromium and render as a widget.
just started, and hoping to perfect this and inspire whatsapp to provide
us with a clean client.
"""

class AngelCommClient(QTabWidget):
    def __init__(self):
        self.view1 = None
        self.view2 = None
        self.view3 = None
        self.view4 = None
        self.view5 = None
        self.view6 = None

        self.img_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "assets/img/"))
        self.APP_TITLE = "Angeldrome Comm Center"

    def setChannels(self):
        if(self.view1 == None and self.settingsWidget.showWhatsapp()):
            self.view1 = ChatViewComponent( "https://web.whatsapp.com", "whatsapp-client", self)
            t1 = self.addTab(self.view1, "WhatsApp")
            self.setTabIcon(t1, QtGui.QIcon(self.img_path + "/whatsapp.png"))

        if(self.view2 == None and self.settingsWidget.showSkype()):
            self.view2 = ChatViewComponent("https://web.skype.com", "skype-client", self)
            t2 = self.addTab(self.view2, "Skype")
            self.setTabIcon(t2, QtGui.QIcon(self.img_path + "/skype.png"))

        if(self.view3 == None and self.settingsWidget.showGoogle()):
            self.view3 = ChatViewComponent("https://mail.google.com", "google-suite", self)
            t3 = self.addTab(self.view3, "Google Suite")
            self.setTabIcon(t3, QtGui.QIcon(self.img_path + "/google.png"))

        if(self.view4 == None and self.settingsWidget.showMDollar()):
            self.view4 = ChatViewComponent("https://outlook.office.com", "ms-outlook", self)
            t4 = self.addTab(self.view4, "Outlook")
            self.setTabIcon(t4, QtGui.QIcon(self.img_path + "/outlook.png"))

        if(self.view5 == None and self.settingsWidget.showSlack()):
            self.view5 = ChatViewComponent("https://www.slack.com", "slack", self)
            t5 = self.addTab(self.view5, "Slack")
            self.setTabIcon(t5, QtGui.QIcon(self.img_path + "/slack.png"))

        if(self.view6 == None and self.settingsWidget.showLinkedin()):
            self.view6 = ChatViewComponent("https://www.linkedin.com", "linkedin", self)
            t5 = self.addTab(self.view6, "LinkedIn")
            self.setTabIcon(t5, QtGui.QIcon(self.img_path + "/linkedin.png"))

    def createApp(self, app):
        self.app = app

    def initializeApp(self):
        self.APP_ICON = QtGui.QIcon(self.img_path + "/angeldrome.png")
        QTabWidget.__init__(self)
        self.app.setWindowIcon(self.APP_ICON)
        self.setWindow()
        self.settingsWidget = SettingsComponent(self)

        t0 = self.addTab(self.settingsWidget, "Settings")

        self.setChannels()
        self.createTray()
        self.hide()
        self.app.exec()

    """
    method createTray
    to configure and enable to show the app icon in system tray.
    somehow this doesnt work in some flavours of desktops of linux and my XFCE is one such :)
    """
    def createTray(self):
        if(QSystemTrayIcon.isSystemTrayAvailable()):
            self.tray = QSystemTrayIcon(self.APP_ICON, self)
            show_action = QAction("Show", self)
            quit_action = QAction("Exit", self)
            hide_action = QAction("Hide", self)

            show_action.triggered.connect(self.show)
            hide_action.triggered.connect(self.hide)
            quit_action.triggered.connect(self.app_quit)

            tray_menu = QMenu()
            tray_menu.addAction(show_action)
            tray_menu.addAction(hide_action)
            tray_menu.addAction(quit_action)
            self.tray.setContextMenu(tray_menu)
            self.tray.activated.connect(self.sys_tray_clicking)
            self.tray.setToolTip("Angeldrome Comm Center")
            self.tray.show()
        else:
            QMessageBox.information(self, "Angel Chat Client", "System Tray Not Available. Docking wont be possible.")
            self.show()

    def app_quit(self):
        try:
            if(self.settingsWidget.timerObject != None):
                self.settingsWidget.timerObject.cancel()
        except:
            pass
        if(self.view1 != None):
            self.view1.page().profile().deleteLater()
            self.view1.setPage(None)
        if(self.view2 != None):
            self.view2.page().profile().deleteLater()
            self.view2.setPage(None)
        if(self.view3 != None):
            self.view3.page().profile().deleteLater()
            self.view3.setPage(None)
        if(self.view4 != None):
            self.view4.page().profile().deleteLater()
            self.view1.setPage(None)
        if(self.view5 != None):
            self.view5.page().profile().deleteLater()
            self.view5.setPage(None)
        self.app.quit()

    def sys_tray_clicking(self, reason):
        if (reason == QSystemTrayIcon.ActivationReason.Trigger):
            if ( self.isHidden() ) :
                self.show()
            else :
                self.hide()
        elif (reason == QSystemTrayIcon.MiddleClick):
            self.close()

    """
    method setWindow
    confgiures the QWidget window with bounds and flags.
    """
    def setWindow(self):
        self.setWindowTitle(self.APP_TITLE)
        self.setWindowIcon(self.APP_ICON)
        self.setWindowFlags( QtCore.Qt.WindowType.WindowCloseButtonHint | QtCore.Qt.WindowType.WindowMinMaxButtonsHint )
        self.setWindowDimensions()

    """
    method setWindowDimensions
    to set the bounds
    """
    def setWindowDimensions(self):
        self.rect = self.screen().availableGeometry()
        self.taskBarHeight = self.screen().availableSize().height() - self.rect.height()
        self.resize(round(self.rect.width()), self.rect.height() - self.taskBarHeight - 20)
        self.move(0,0)

    """
    method changeEvent
    to identify minimizing event and to hide the app and dock to right.
    """
    def changeEvent(self, event):
        if event.type() == QEvent.Type.WindowStateChange:
            if self.windowState() & Qt.WindowMinimized:
                event.ignore()
                self.hide()

    """
    method closeEvent
    to identify app close trigger from widget and ignore to hide it. Let app be closed from tray alone
    """
    def closeEvent(self, event):
        event.ignore()
        self.hide()

if __name__ == "__main__":
    client = AngelCommClient()
    app = QApplication(["AngelChat"])
    client.createApp(app)
    client.initializeApp()
    sys.exit()
