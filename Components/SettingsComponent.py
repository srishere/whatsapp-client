import sys, threading, socket, os.path, requests, json
from datetime import datetime, timezone, timedelta

from PyQt6 import QtCore
from PyQt6.QtWidgets import QSystemTrayIcon, QApplication, QMenu, QMessageBox, QTabWidget, QWidget, QCheckBox, QLineEdit, QPushButton, QMessageBox, QApplication, QGridLayout, QLabel
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QIcon, QAction

from .partials.TimeZonePanel import TimeZonePanel
from .partials.WeatherPanel import WeatherPanel

class SettingsComponent(QWidget):
    __config = os.path.abspath(os.path.join(os.path.dirname(__file__), "../secrets/angel.cfg"))

    def __init__(self, mainObject):
        super().__init__()
        self.conf = None
        self.mainObject = mainObject
        self.noNetButton = None
        if(self.checkNet("https://www.angeldrome.com")):
            if(os.path.isfile(self.__config)):
                fObj = open(self.__config, "r")
                self.conf = fObj.read().strip("\n").split('|')
                fObj.close()
            self.initSettings()
        else:
            self.showNoNetMessage()


    def showNoNetMessage(self):
        self.noNetButton = QPushButton('No Network. Please Click This Button Once Net Is Restored.', self)
        self.noNetButton.setGeometry(QtCore.QRect(150, 300, 400, 20))
        self.noNetButton.clicked.connect(self.restoreApp)

    def restoreApp(self):
        if(self.checkNet("https://www.angeldrome.com")):
            self.noNetButton.deleteLater()
            self.noNetButton = None
            self.initSettings()

    def checkNet(self, url):
        try:
            socket.create_connection((url.replace("https://",""), 443))
            return True
        except OSError:
            return False

    def initSettings(self):
        self.grid = QGridLayout()

        self.drawMainSettings()
        self.setWeather()
        self.setTZ()
        self.grid.addWidget(QLabel(""), 9, 0)
        self.grid.addWidget(QLabel(""), 10, 0)
        self.grid.addWidget(QLabel(""), 11, 0)
        self.grid.addWidget(QLabel(""), 12, 0)
        self.setLayout(self.grid)

    def drawMainSettings(self):
        self.check_whatsapp = QCheckBox('Whatsapp', self)
        self.grid.addWidget(self.check_whatsapp, 1, 0)

        self.check_skype = QCheckBox('Skype', self)
        self.grid.addWidget(self.check_skype, 2, 0)

        self.check_gmail = QCheckBox('Google', self)
        self.grid.addWidget(self.check_gmail, 3, 0)

        self.check_slack = QCheckBox('Slack', self)
        self.grid.addWidget(self.check_slack, 4, 0)

        self.check_linkedin = QCheckBox('Linkedin', self)
        self.grid.addWidget(self.check_linkedin, 5, 0)

        self.check_ga = QCheckBox('Google Assistant (not yet. dont use)', self)
        self.grid.addWidget(self.check_ga, 6, 0)
        self.check_ga.stateChanged.connect(self.enableGA)

        self.g_path = QLineEdit(self)
        self.g_path.setPlaceholderText("Provide absolute path of auth json file.")
        self.g_path.hide()
        self.grid.addWidget(self.g_path, 6, 1)
        self.g_path.resize(400,20)

        if(self.conf != None):
            if(self.conf[0] == "Y"):
                self.check_whatsapp.setChecked(True)
            if(self.conf[1] == "Y"):
                self.check_skype.setChecked(True)
            if(self.conf[2] == "Y"):
                self.check_gmail.setChecked(True)
            if(self.conf[3] == "Y"):
                self.check_mdollar.setChecked(True)
            if(self.conf[4] == "Y"):
                self.check_slack.setChecked(True)
            if(self.conf[5] == "Y"):
                self.check_linkedin.setChecked(True)
            if(self.conf[6] == "Y"):
                self.check_ga.setChecked(True)
                self.g_path.setText(self.conf[4])

        self.s_button = QPushButton('Apply Settings', self)
        self.grid.addWidget(self.s_button, 7, 0)
        self.s_button.clicked.connect(self.onSettings)

    def setWeather(self):
        self.weatherPanel = WeatherPanel()
        self.grid.addWidget(self.weatherPanel.stack, 8, 0)

    def setTZ(self):
        self.tzPanel = TimeZonePanel()
        self.grid.addWidget(self.tzPanel.stack, 8, 2)

    def showWhatsapp(self):
        return ( self.conf != None and self.conf[0] == "Y" )

    def showSkype(self):
        return ( self.conf != None and self.conf[1] == "Y" )

    def showGoogle(self):
        return ( self.conf != None and self.conf[2] == "Y" )

    def showMDollar(self):
        return ( self.conf != None and self.conf[3] == "Y" )

    def showSlack(self):
        return ( self.conf != None and self.conf[4] == "Y" )

    def showLinkedin(self):
        return ( self.conf != None and self.conf[5] == "Y" )

    def showGoogleAssistant(self):
        return ( self.conf != None and self.conf[6] == "Y" )

    def onSettings(self):
        aPath = ""

        if(self.check_ga.isChecked()):
            aPath = self.g_path.text()
            if(not os.path.isfile(aPath)):
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setText("Please provide a valid file.")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
                return

        whatsappOn = "Y" if self.check_whatsapp.isChecked() else "N"
        skypeOn = "Y" if self.check_skype.isChecked() else "N"
        googleOn = "Y" if self.check_gmail.isChecked() else "N"
        gassistOn = "Y" if self.check_ga.isChecked() else "N"
        mdollarOn = "N"
        slackOn = "Y" if self.check_slack.isChecked() else "N"
        linkedinOn = "Y" if self.check_linkedin.isChecked() else "N"
        settings_string = whatsappOn + "|" + skypeOn + "|" + googleOn + "|" + mdollarOn  + "|" + slackOn + "|" + linkedinOn + "|" + gassistOn
        self.conf = settings_string.split("|")
        fObj = open(self.__config, "w")
        fObj.write(settings_string)
        fObj.close()
        self.mainObject.setChannels()

    def enableGA(self, state):
        if state == Qt.Checked:
            self.g_path.show()
        else:
            self.g_path.setText("")
            self.g_path.hide()
