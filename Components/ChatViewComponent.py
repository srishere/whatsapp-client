import os, socket

from PyQt6 import QtCore, QtWidgets, QtWebEngineWidgets, QtGui
from PyQt6.QtWebEngineWidgets import *
from PyQt6.QtWidgets import QPushButton
from PyQt6.QtCore import *
from PyQt6.QtWebEngineCore import *

class ChatViewPage(QWebEnginePage):
    
    def createWindow(self, _type):
        page = QWebEnginePage(self.profile(), self)
        page.profile().setHttpUserAgent("Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.5414.74 Safari/537.36")
        page.urlChanged.connect(self.on_url_changed)
        return page

    @QtCore.pyqtSlot(QtCore.QUrl)
    def on_url_changed(self, url):
        page = self.sender()
        self.setUrl(url)
        page.deleteLater()

class ChatViewComponent(QWebEngineView):

    def __init__(self, url, profile_name, baseObject = None):
        self.baseObject = baseObject;
        self.CACHE_DIR_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), "../cache/"+profile_name+"/"))
        self.HEADER_UA = "Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.5414.74 Safari/537.36"
        QWebEngineView.__init__(self)
        self.tab = self.parent()
        self.setChatPage(url, profile_name)

    """
    method setChatPage
    confgiures the browser with customized header and renders web whatsapp.
    this further registers a callback for permission requests and accepts them.
    though i haven't seen this working yet :)
    this customized header was required to allow web whatsapp to accept the chromium browser
    that we are using here.
    """
    def setChatPage(self, url, profile_name):
        profile = QWebEngineProfile(profile_name, self)
        profile.setPersistentStoragePath(self.CACHE_DIR_PATH)
        profile.setHttpUserAgent(self.HEADER_UA)
        profile.clearHttpCache()
        profile.downloadRequested.connect( self.onDownloadRequested )
        self.webpage = ChatViewPage(profile, self)
        self.setPage(self.webpage)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.FullScreenSupportEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.DnsPrefetchEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.FocusOnNavigationEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.PlaybackRequiresUserGesture, False)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.JavascriptEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.ShowScrollBars, True)
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.AutoLoadImages, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.JavascriptEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.JavascriptCanOpenWindows, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.JavascriptCanAccessClipboard, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.LinksIncludedInFocusChain, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.LocalStorageEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.XSSAuditingEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.SpatialNavigationEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.LocalContentCanAccessFileUrls, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.HyperlinkAuditingEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.ScrollAnimatorEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.ErrorPageEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.PluginsEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.FullScreenSupportEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.ScreenCaptureEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.WebGLEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.Accelerated2dCanvasEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.HyperlinkAuditingEnabled, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.AutoLoadIconsForPage, True);
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.TouchIconsEnabled, True);
        if(self.checkNet(url) == True):
            self.webpage.featurePermissionRequested.connect(self.onFeaturePermissionRequested)
            self.webpage.setFeaturePermission(QUrl(url), QWebEnginePage.Feature.Notifications, QWebEnginePage.PermissionPolicy.PermissionGrantedByUser)
            self.load(QUrl(url))
        else:
            self.noNetButton = QPushButton('No Network. Please Click This Button Once Net Is Restored.', self)
            self.noNetButton.setGeometry(QtCore.QRect(300, 300, 400, 20))
            self.noNetButton.clicked.connect(self.baseObject.settingsWidget.restoreApp)

    def checkNet(self, url):
        try:
            socket.create_connection((url.replace("https://",""), 443))
            return True
        except OSError:
            print(OSError)
            return False



    """
    method onFeaturePermissionRequested
    handler to cater incoming feature permission requested.
    """
    def onFeaturePermissionRequested(self, url, feature):
        self.webpage.setFeaturePermission(QUrl(url), feature, QWebEnginePage.PermissionPolicy.PermissionGrantedByUser)

    def onDownloadRequested(self, download):
        old_path = download.url().path()
        suffix = QtCore.QFileInfo(old_path).suffix()
        # TODO:: having an issue here with the file name. need to check mime etc and generate a file extn suffix i guess...
        path, _ = QtWidgets.QFileDialog.getSaveFileName(
            self, "Save File", old_path, "*." + suffix
        )
        if path:
            download.setPath(path)
            download.accept()

