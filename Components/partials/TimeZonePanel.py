import os.path, json
from datetime import datetime, timezone, timedelta

from PyQt6 import QtCore
from PyQt6.QtWidgets import QStackedWidget, QWidget, QPushButton, QGridLayout, QLabel, QComboBox

class TimeZonePanel(QWidget):
    __countries_file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../assets/data/countries_tz.json"))
    __config = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../secrets/tz.cfg"))
    __data = []
    __items = []

    def __init__(self):
        super().__init__()
        self.style = '''
            QWidget {
                background-color: #ADD8E6;
                border: 1px solid #0000FF;
            }
            QLabel {
                background-color: transparent;
                border: 0px solid transparent;
            }
            '''
        self.stack = QStackedWidget(self)
        self.displayWidget = QWidget(self)
        self.editWidget = QWidget(self)
        self.stack.addWidget(self.displayWidget)
        self.stack.addWidget(self.editWidget)
        self.createDisplayWidget()
        self.createEditWidget()

    def createTZItem(self, idx, label_text, offset):
        lbl = QLabel("<b>"+label_text+"</b>")
        self.grid.addWidget(lbl, idx + 3, 1)
        txt = QLabel()
        self.grid.addWidget(txt, idx + 3, 2)
        txt.setText(datetime.now(timezone(timedelta(hours=offset), label_text)).strftime('%Y:%m:%d %H:%M:%S'))
        return {"label":lbl, "text":txt, "label_text": label_text, "offset":offset}

    def createDisplayWidget(self):
        self.displayWidget.setStyleSheet(self.style)
        self.grid = QGridLayout()
        self.grid.addWidget(QLabel("<h1>Time Now At:</h1>"), 2, 1)
        self.editButton = QPushButton("Edit", self)
        self.editButton.clicked.connect(self.showEdit)
        self.grid.addWidget(self.editButton, 2, 3)
        with open(self.__countries_file_path, 'r') as cntries:
            self.__countries = json.loads(cntries.read())
        self.__items.append(self.createTZItem(0, "UTC", 0))
        self.__data.append({"name":"UTC","offset":0})
        if(os.path.isfile(self.__config)):
            fObj = fObj = open(self.__config, "r")
            conf = fObj.read().strip("\n").split(',')
            for idx, cfg in enumerate(conf):
                pair_value = cfg.split('|')
                self.__data.append({"name":pair_value[0],"offset":float(pair_value[1])})
                self.__items.append(self.createTZItem(idx+1, pair_value[0], float(pair_value[1])))
        self.rows = len(self.__items)
        for idx in range(self.rows, 15):
            self.grid.addWidget(QLabel(""), idx + 1, 1)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.draw)
        self.timer.start(1000) # every second.
        self.draw()
        self.displayWidget.setLayout(self.grid)

    def createEditWidget(self):
        self.editWidget.setStyleSheet(self.style)
        self.editGrid = QGridLayout()
        self.editGrid.addWidget(QLabel("<h1>Select Countries for Time Display:</h1>"), 0, 1)
        self.cntryBox = QComboBox()
        self.selectedCntries = QWidget()
        self.selectedCntries.setLayout(QGridLayout())
        self.drawSelectedCountries()
        self.cntryBox.addItem("--Select A Country--")
        for cntry in self.__countries:
            self.cntryBox.addItem(cntry["name"])
        self.cntryBox.currentIndexChanged.connect(self.addCountry)
        self.editGrid.addWidget(self.cntryBox, 1, 1)
        self.editGrid.addWidget(self.selectedCntries, 1, 3)
        self.saveButton = QPushButton("Back", self)
        self.saveButton.clicked.connect(self.closeEdit)
        self.editGrid.addWidget(self.saveButton, 12, 4)
        self.editWidget.setLayout(self.editGrid)

    def drawSelectedCountries(self):
        current_layout = self.selectedCntries.layout()
        for i in reversed(range(current_layout.count())):
            current_layout.itemAt(i).widget().deleteLater()
            current_layout.itemAt(i).widget().setParent(None)
        for idx, selected in enumerate(self.__data):
            if idx > 0:
                btn = QPushButton(selected["name"]+"(X)")
                btn.setObjectName("b_"+str(idx))
                btn.clicked.connect(self.deleteCountry)
            else:
                btn = QPushButton(selected["name"])
            current_layout.addWidget(btn, idx, 0)

    def deleteCountry(self):
        self.timer.stop()
        btn = self.sender()
        name = btn.objectName().split("_")
        index = int(name[1])
        del self.__data[index]
        self.__items = []
        self.saveData()
        self.drawSelectedCountries()
        self.reDraw()
        self.timer.start(1000)
        btn.deleteLater()

    def reDraw(self):
        current_layout = self.displayWidget.layout()
        for i in reversed(range(current_layout.count())):
            current_layout.itemAt(i).widget().deleteLater()
            current_layout.itemAt(i).widget().setParent(None)
        current_layout.addWidget(QLabel("<h1>Time Now At:</h1>"), 2, 1)
        self.editButton = QPushButton("Edit", self)
        self.editButton.clicked.connect(self.showEdit)
        current_layout.addWidget(self.editButton, 2, 3)
        for idx, d in enumerate(self.__data):
            self.__items.append(self.createTZItem(idx, d["name"], float(d["offset"])))
        self.rows = len(self.__items)
        for idx in range(self.rows, 15):
            current_layout.addWidget(QLabel(""), idx + 1, 1)

    def addCountry(self, index):
        if index > 0:
            cntry = self.__countries[index - 1]
            self.__items.append(self.createTZItem(len(self.__data), cntry["name"], float(cntry["value"])))
            self.__data.append({"name":cntry["name"],"offset":float(cntry["value"])})
            self.saveData()
            self.drawSelectedCountries()
            self.cntryBox.setCurrentIndex(0)

    def saveData(self):
        settings_string = ""
        for idx, cntry in enumerate(self.__data):
            if idx > 0:
                if idx > 1:
                    settings_string += ","
                settings_string += cntry["name"] + "|" + str(cntry["offset"])
        fObj = open(self.__config, "w")
        fObj.write(settings_string)
        fObj.close()

    def showEdit(self):
        self.draw()
        self.stack.setCurrentIndex(1)

    def closeEdit(self):
        self.stack.setCurrentIndex(0)

    def draw(self):
        for idx, item in enumerate(self.__items):
            item["text"].setText(datetime.now(timezone(timedelta(hours=item["offset"]), item["label_text"])).strftime('%Y:%m:%d %H:%M:%S'))
            item["label"].adjustSize()
            item["text"].adjustSize()
