import os.path, requests, json
from datetime import datetime, timezone, timedelta

from PyQt6 import QtCore
from PyQt6.QtWidgets import QStackedWidget, QWidget, QPushButton, QGridLayout, QLabel, QLineEdit
from PyQt6.QtGui import QPixmap
from PIL import Image
from PIL.ImageQt import ImageQt

#TODO: temporary. will get rid of these and get latest cert chains instead
from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
#End of temporary imports


class WeatherPanel(QWidget):
    __config_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../secrets/meteo.cfg"))
    __cfgdata = None
    __onecall_prefix = "https://api.open-meteo.com/v1/forecast"
    __location_name = "India"
    __img_prefix = "assets/img/weather-icons"

    __wmo_codes = {
        "clear" : [0],
        "part-cloud" : [1,2,3],
        "fog" : [45,48],
        "drizzle" : [51,53,55,56,57],
        "rain" : [61,63,65,66,67],
        "snow" : [71,73,75,77,85,86],
        "thunderstorm" : [80,81,82,95,96,99]
    }

    __conditions_for_display = {
        "clear" : "Sky is Clear",
        "part-cloud" : "Partly Cloudy",
        "fog" : "Fog",
        "drizzle" : "Slight Drizzle",
        "rain" : "Raining",
        "snow" : "Snowing",
        "thunderstorm" : "Thunder Storm"
    }

    def __init__(self):
        super().__init__()
        self.FIFTEEN_MINUTES = 900000;
        self.style = '''
            QWidget {
                background-color: #ADD8E6;
                border: 1px solid #0000FF;
            }
            QLabel {
                background-color: transparent;
                border: 0px solid transparent;
            }
            '''
        self.stack = QStackedWidget(self)
        self.displayWidget = QWidget(self)
        self.editWidget = QWidget(self)
        self.stack.addWidget(self.displayWidget)
        self.stack.addWidget(self.editWidget)
        self.createDisplayWidget()
        self.createEditWidget()

    def createDisplayWidget(self):
        self.__getWeatherConfig()
        self.grid = QGridLayout()
        self.displayWidget.setStyleSheet(self.style)
        if(self.__cfgdata != None and isinstance(self.__cfgdata, list) and len(self.__cfgdata) == 4):
            location_label = self.__location_name
            data = self.__getWeatherData()
            if data:
                self.weatherHeaderLbl = QLabel()
                self.conditionLbl = QLabel(data["main_weather"])
                self.currentTemperatureLbl = QLabel(data["current_temperature"])
                self.feelsLikeLbl = QLabel(data["feels_like"])
                self.minTemperatureLbl = QLabel(data["min_temperature"])
                self.maxTemperatureLbl = QLabel(data["max_temperature"])
                self.sunriseLbl = QLabel(data["sunrise"])
                self.sunsetLbl = QLabel(data["sunset"])

                disable_warnings(InsecureRequestWarning)
                im = Image.open(data["img"])
                qp = QPixmap.fromImage(ImageQt(im))
                self.weatherHeaderLbl.setPixmap(qp)
                self.locationNameLbl = QLabel("<h1>Current Weather: "+location_label+"</h1>")
                self.grid.addWidget(self.weatherHeaderLbl, 1, 4)
                self.grid.addWidget(self.locationNameLbl, 1, 3 )
                self.editButton = QPushButton("Edit", self)
                self.editButton.clicked.connect(self.showEdit)
                self.grid.addWidget(self.editButton, 1, 5)
                self.grid.addWidget(QLabel("<b>Condition</b>"), 2, 3)
                self.grid.addWidget(self.conditionLbl, 2, 4)
                self.grid.addWidget(QLabel("<b>Current Temperature</b>"), 3, 3)
                self.grid.addWidget(self.currentTemperatureLbl, 3, 4)
                self.grid.addWidget(QLabel("<b>Feels Like</b>"), 4, 3)
                self.grid.addWidget(self.feelsLikeLbl, 4, 4)
                self.grid.addWidget(QLabel("<b>Minimum Temperature</b>"), 5, 3)
                self.grid.addWidget(self.minTemperatureLbl, 5, 4)
                self.grid.addWidget(QLabel("<b>Maximum Temperature</b>"), 6, 3)
                self.grid.addWidget(self.maxTemperatureLbl, 6, 4)
                self.grid.addWidget(QLabel("<b>Sunrise</b>"), 7, 3)
                self.grid.addWidget(self.sunriseLbl, 7, 4)
                self.grid.addWidget(QLabel("<b>Sunset</b>"), 8, 3)
                self.grid.addWidget(self.sunsetLbl, 8, 4)
                self.grid.addWidget(QLabel(""), 9, 3)
                self.grid.addWidget(QLabel(""), 10, 3)
                self.grid.addWidget(QLabel(""), 11, 3)
                self.grid.addWidget(QLabel(""), 12, 3)
                self.grid.addWidget(QLabel(""), 13, 3)
                self.grid.addWidget(QLabel(""), 14, 3)
                self.timer = QtCore.QTimer(self)
                self.timer.timeout.connect(self.__reDraw)
                self.timer.start(self.FIFTEEN_MINUTES)
        self.displayWidget.setLayout(self.grid)

    def createEditWidget(self):
        self.editWidget.setStyleSheet(self.style)
        self.editGrid = QGridLayout()
        self.locnField = QLineEdit(self)
        self.locnField.setPlaceholderText("Location Name.")
        self.editGrid.addWidget(self.locnField, 2, 1)
        self.locnField.resize(400,20)

        self.latField = QLineEdit(self)
        self.latField.setPlaceholderText("Location Latitute.")
        self.editGrid.addWidget(self.latField, 3, 1)
        self.latField.resize(400,20)

        self.lngField = QLineEdit(self)
        self.lngField.setPlaceholderText("Location Longitute.")
        self.editGrid.addWidget(self.lngField, 4, 1)
        self.lngField.resize(400,20)
        self.saveButton = QPushButton("Save", self)
        self.saveButton.clicked.connect(self.closeEdit)
        self.editGrid.addWidget(self.saveButton, 5, 3)

        if(self.__cfgdata != None and isinstance(self.__cfgdata, list) and len(self.__cfgdata) == 4):
            self.latField.setText(self.__cfgdata[0])
            self.lngField.setText(self.__cfgdata[1])
            self.locnField.setText(self.__cfgdata[3])

        self.editWidget.setLayout(self.editGrid)

    def __reDraw(self):
        data = self.__getWeatherData()
        if ( data != False ):
            location_label = self.__location_name
            self.locationNameLbl.setText("<h1>Current Weather: "+location_label+"</h1>")
            self.conditionLbl.adjustSize()
            disable_warnings(InsecureRequestWarning)
            im = Image.open(data["img"])
            qp = QPixmap.fromImage(ImageQt(im))
            self.weatherHeaderLbl.setPixmap(qp)
            self.weatherHeaderLbl.adjustSize()

            self.conditionLbl.setText(data["main_weather"])
            self.conditionLbl.adjustSize()

            self.currentTemperatureLbl.setText(data["current_temperature"])
            self.currentTemperatureLbl.adjustSize()

            self.feelsLikeLbl.setText(data["feels_like"])
            self.feelsLikeLbl.adjustSize()

            self.minTemperatureLbl.setText(data["min_temperature"])
            self.minTemperatureLbl.adjustSize()

            self.maxTemperatureLbl.setText(data["max_temperature"])
            self.maxTemperatureLbl.adjustSize()

            self.sunriseLbl.setText(data["sunrise"])
            self.sunriseLbl.adjustSize()

            self.sunsetLbl.setText(data["sunset"])
            self.sunsetLbl.adjustSize()

    def showEdit(self):
        self.stack.setCurrentIndex(1)

    def closeEdit(self):
        lat = self.latField.text()
        lng = self.lngField.text()
        locn = self.locnField.text()

        if(locn != "" and lat != "" and lng != ""):
            settings_string = lat + "|" + lng + "|" + self.__onecall_prefix + "|" + locn
            fObj = open(self.__config_path, "w")
            fObj.write(settings_string)
            fObj.close()
        self.stack.setCurrentIndex(0)

    def __getWeatherConfig(self):
        if(os.path.isfile(self.__config_path)):
            fObj = open(self.__config_path, "r")
            self.__cfgdata = fObj.read().strip("\n").split('|')
            self.__onecall_prefix = self.__cfgdata[2]
            self.__location_name = self.__cfgdata[3]
            fObj.close()

    def __getIconPath(self, wmo_code, is_day):
        result = {}
        filename_suffix = "-day" if is_day == 1 else "-night"
        for key, codes in self.__wmo_codes.items():
            if(wmo_code in codes):
                result["key_text"] = self.__conditions_for_display[key]
                result["file_name"] = key+filename_suffix+".png"
        if(not result):
            result = {"key_text":"clear", "file_name":"clear"+filename_suffix+".png"}
        return result

    def __getWeatherData(self):
        data = False

        try:
            url = self.__onecall_prefix+"?latitude="+self.__cfgdata[0]+"&longitude="+self.__cfgdata[1]+"&current=temperature_2m,is_day,apparent_temperature,rain,showers,snowfall,weather_code&daily=temperature_2m_max,temperature_2m_min,sunrise,sunset&timezone=auto&forecast_days=1"
            resp = requests.get(url)
            if(resp.status_code == 200):
                data = resp.json()
            else:
                data = False
        except:
            print("failed to get response")
            data = False
        try:
            if(data != False):
                icon_result = self.__getIconPath(data["current"]["weather_code"], data["current"]["is_day"])
                return {
                    "img" : self.__img_prefix+"/"+icon_result["file_name"],
                    "main_weather" : str(data["current"]['temperature_2m']) + " ℃ (" + icon_result["key_text"] + ")",
                    "current_temperature" : str(data["current"]['temperature_2m']) +" ℃",
                    "feels_like" : str(data["current"]['apparent_temperature']) +" ℃",
                    "min_temperature" : str(data["daily"]['temperature_2m_min'][0]) +" ℃",
                    "max_temperature" : str(data["daily"]['temperature_2m_max'][0]) +" ℃",
                    "sunrise" : str(data["daily"]['sunrise'][0]),
                    "sunset" : str(data["daily"]['sunset'][0])
                }
        except:
            print(data)
            data = False
        return False
