﻿[![Angeldrome Comm Center](https://www.angeldrome.com/img/logo.png)](https://www.angeldrome.com/)
# An Attempt to keep as my development related communication center.

Started this as a whatsapp client, and now am adding more to this. Google Assistant is not there yet. will get around that.
Now i can choose to have google mail (as it has calendar, task and keep in web interface, with google meet)
skype as some clients prefer that
and whatsapp that i use a lot for communication

# Came across few things

 1. chromium UA header passed by default was allergic to web whatsapp, so had to tweak it a bit. All works good.
 2. Not sure about notifications. have added code to enable notifications, but havent seen anything yet.
    may be https://github.com/QupZilla/qtwebkit-plugins is a solution.

## How to Deploy
clone / download this code base, and ensure that the four required files are made present in same directory.

 angel_chat.py is the executable. Ensure on keeping all files available relative to this file.
 used pyinstaller to create an executable in debian and it works good.
 only thing is the directories such as secrets, assets, static need to me kept inside the directory where the executable is available.
 will get a way to make all these bundled as an installer soon.

 example:: nohup <relative or absolute path>/angel_chat.py & 1>&2

 In secrets directory, if you have a meteo.cfg file having a single line of entry like below<br />
 lat|long|https://api.open-meteo.com/v1/forecast|location_name<br />
 example::63.4641|142.7737|https://api.open-meteo.com/v1/forecast|Oymyakon<br />
 then you will see weather in settings area.<br />
 reference: https://open-meteo.com/en/docs<br />

## TODO

 - Extend this as a XFCE plugin
